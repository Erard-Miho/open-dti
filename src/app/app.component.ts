
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FindIpService } from './core/IP/find-ip.service';
import { ipUser } from './authentication/User/ipUser.model';
@Component({
  selector: 'app-root',
  template:`<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit {
  constructor(private IP: FindIpService, private router: Router){}


  ngOnInit(){
    this.IP.getLocation().subscribe((respone:any) =>{
      // response.ip
      this.IP.getAllIpInfo(respone.ip).subscribe((res:any) =>{
        // to get the country res.location.country.name
        
        var json = res;
        var keys = Object.values(json);
         var ipuser ={
           "ip":res.ip,
           "type":res.type,
           "hostname":res.hostname,
           "carrier":res.carrier,
           "connection":res.connection,
           "currency":res.currency,
           "location":res.location,
           "security":res.security,
           "time_zone":res.time_zone
         }

         this.IP.PostIpInfo(ipuser).subscribe((res)=>{});
        
        //this.IP.PostIpInfo()
        if(res.location.country.name != 'Albania'){
          this.router.navigate(['err']);
        }
      })
    })
  }
}

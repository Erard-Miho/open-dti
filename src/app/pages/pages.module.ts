import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { FashionAndClothesComponent } from './fashion-and-clothes/fashion-and-clothes.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';


@NgModule({
  declarations: [HomeComponent, FashionAndClothesComponent, NavBarComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    HttpClientModule
  ]
})
export class PagesModule { }

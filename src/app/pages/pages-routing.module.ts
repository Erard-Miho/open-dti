import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FashionAndClothesComponent } from './fashion-and-clothes/fashion-and-clothes.component';
const routes: Routes = [
  {
    path:'home',
    component: HomeComponent
  },
  {
    path:'fashion-and-clothes',
    component: FashionAndClothesComponent
  },
  {
    path: '',
    redirectTo: 'fashion-and-clothes',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }

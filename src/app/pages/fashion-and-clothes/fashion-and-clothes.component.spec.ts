import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionAndClothesComponent } from './fashion-and-clothes.component';

describe('FashionAndClothesComponent', () => {
  let component: FashionAndClothesComponent;
  let fixture: ComponentFixture<FashionAndClothesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FashionAndClothesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionAndClothesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

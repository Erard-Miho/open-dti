

export interface ipUser {

    ip: string;
    type: string;
    hostname: string;
    carrier: string;
    connection: string;
    currency:string ;
    location: string;
    security: string;
    time_zone: string;
  }
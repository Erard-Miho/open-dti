import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
//import { DomSanitizer } from '@angular/platform-browser';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{
  closeResult = '';
  v=false; // the condition for the 'active' class to blur
  varb='';
  constructor(
    private modalService: NgbModal, 
    private router:Router, 
    private render: Renderer2, 
    private el: ElementRef,
    ) {

  }
  //private modalRef: NgbModalRef;
  // destroy open(content)
  qwe(w){

    //this.modalRef.close();
    this.modalService.dismissAll();
  }
  open(content) {
    this.v=true;
    // ariaLabelledBy: 'modal-basic-title'
  //   this.modalRef = this.modalService.open(content,{centered:true});
  // this.modalRef.result.then((result) => {
  //     this.closeResult = `Closed with: ${result}`;

  //   }, (reason) => {
  //     this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

  //   });


    this.modalService.open(content, {centered: true}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

    });

  }
  private getDismissReason(reason: any): string {

    if (reason === ModalDismissReasons.ESC) {
      this.v=false;
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      this.v=false;
      return 'by clicking on a backdrop';
    }
     else {
      return `with: ${reason}`;
    }
  }

  onClick(content) {
    this.v=true;

  }



  ngOnInit(): void {
 
  }

  temp(){
    this.render.setStyle(this.el.nativeElement,'margin','500px');
  }


}

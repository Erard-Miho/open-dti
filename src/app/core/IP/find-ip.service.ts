import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ipUser } from '../../authentication/User/ipUser.model';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class FindIpService {

  constructor(private http: HttpClient) { }

  public getLocation(){
    return this.http.get("https://api.ipify.org/?format=json");
  }

  public getAllIpInfo(ip:string){
    return this.http.get(`https://api.ipregistry.co/${ip}?key=w90rb8fe66wvrt`);
  }

  // public PostIpInfo(ip:string){
  //   public signup(user: NewUser): Observable<any> {
      // We're using the spread syntax to get the
      // individual properties off the supplied user
      // object onto a new object
  //     return this.http.post(`http://localhost:1337/users`, { ...user });
  //   }
  // }
  public PostIpInfo(ip: ipUser): Observable<any>{
    return this.http.post(`http://localhost:1337/ip`, { ...ip })
  }
}

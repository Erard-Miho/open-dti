import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(public http: HttpClient) {}

  public checkEmail(email: string): Observable<any> {
    const params = new HttpParams({
      fromObject: {
        email
      }
    });
    return this.http.get(`http://localhost:1337/users`, { params });
  }

  public checkUsername(username: string): Observable<any> {
    const params = new HttpParams({
      fromObject: {
        username
      }
    });
    return this.http.get(`http://localhost:1337/users`, { params });
  }
}
